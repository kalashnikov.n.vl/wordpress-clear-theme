<?php get_header(); ?>
<div id="content" class="site-content">
  <?php if (have_posts()) : ?>
    <header class="page-header">
      <h1 class="page-title"><?php printf(__('Search Results for: %s', THEME_NAME), '<span>' . get_search_query() . '</span>'); ?></h1>
    </header>
    <?php
    while (have_posts()) : the_post();
      convertme_template_part('content', 'search');
    endwhile;
    the_posts_navigation();
  else :
    convertme_template_part('content', 'none');
  endif;
  ?>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
