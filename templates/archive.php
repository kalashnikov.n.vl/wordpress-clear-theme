<?php get_header(); ?>
<div id="content" class="site-content">
  <?php if (have_posts()) : ?>
    <header class="page-header">
      <h1 class="page-title"><?php the_archive_title(); ?></h1>
      <?php the_archive_description('<div class="archive-description">', '</div>'); ?>
    </header>
    <?php
    while (have_posts()) : the_post();
      convertme_template_part('content', get_post_format());
    endwhile;
    the_posts_navigation();
  else :
    convertme_template_part('content', 'none');
  endif;
  ?>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
