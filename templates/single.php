<?php get_header(); ?>
<div id="content" class="site-content">
  <?php
    while (have_posts()) : the_post();
      convertme_template_part('content', get_post_format());
      the_post_navigation();
      if (comments_open() || get_comments_number()) :
        comments_template();
      endif;
    endwhile;
  ?>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
