<?php
/**
 * Functions and definitions
 *
 *
 * @package ConvertMe
 * @since ConvertMe 1.0
 */

	define('THEME_NAME', 'convertme');
	define('THEME_TEMPLATES_DIR', __DIR__ . '/templates');

/**
 * Register block styles.
 */

function convertme_setup() {
	// Добавление поддержки различных функций темы
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');
	add_theme_support('automatic-feed-links');
	add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));
	add_theme_support('custom-logo');
	add_theme_support('customize-selective-refresh-widgets');
	add_theme_support('align-wide');
	add_theme_support('editor-styles');
	add_theme_support('wp-block-styles');
	add_theme_support('responsive-embeds');
	
	// Регистрация меню
	register_nav_menus(array(
		'primary' => __('Primary Menu', THEME_NAME),
		'footer'  => __('Footer Menu', THEME_NAME),
	));

	// Регистрация сайдбаров
	function convertme_widgets_init() {
			register_sidebar(array(
				'name'          => __('Сайдбар', THEME_NAME),
				'id'            => 'sidebar-1',
				'description'   => __('Добавьте виджет в боковую панель.', THEME_NAME),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			));
	}
	add_action('widgets_init', 'convertme_widgets_init');
}
add_action('after_setup_theme', 'convertme_setup');

// Подключение стилей и скриптов
function convertme_scripts() {
	wp_enqueue_style('convertme-style', get_template_directory_uri().'/frontend/dist/css/style.css', array(), '1.0.0', true);
	wp_enqueue_script('convertme-scripts', get_template_directory_uri() . '/frontend/dist/js/app.bundle.js', array(), '1.0.0', true);
}
add_action('wp_enqueue_scripts', 'convertme_scripts');

// Подключение частей шаблона
function convertme_template_part($slug, $name = null) {
	$templates = array();
	if (isset($name)) {
		$templates[] = "part/{$slug}-{$name}.php";
	}
	$templates[] = "part/{$slug}.php";
	locate_template($templates, true, false);
}

// Подключение шаблонов
function convertme_template_include($template) {
	if (is_home() || is_front_page()) {
		$template = THEME_TEMPLATES_DIR . '/index.php';
	} elseif (is_archive()) {
		$template = THEME_TEMPLATES_DIR . '/archive.php';
	} elseif (is_single()) {
		$template = THEME_TEMPLATES_DIR . '/single.php';
	} elseif (is_page()) {
		$template = THEME_TEMPLATES_DIR . '/page.php';
	} elseif (is_search()) {
		$template = THEME_TEMPLATES_DIR . '/search.php';
	} elseif (is_404()) {
		$template = THEME_TEMPLATES_DIR . '/404.php';
	}

	return $template;
}
add_filter('template_include', 'convertme_template_include');

require get_template_directory() . '/app/init.php';