<?php

/**
 * Email template
 * 
 */

  $email_data = $args['email_data'] ?? '';

  $title = __('Заявка с сайта', THEME_NAME);
  $body_title = __('Информация о клиенте', THEME_NAME);
  $date_title =  __('Дата отправления:', THEME_NAME);
  $thanks_message = __('Спасибо, что вы с нами', THEME_NAME);
  $team_title = __('С уважением, Digital-Team ', THEME_NAME);

  $company_logo = 'https://convertme.ru/assets/logo_cm.png';
  $company_link = 'https://convertme.ru/?utm_source=send_form_request';
?>

<?php if(!empty($email_data)):?>
  <div style="padding: 32px;background:#f6f5f5;font-family:Verdana,Arial,sans-serif;margin:0;min-height:100%;height:100%;outline:0;padding:0;text-align:center" bgcolor="#F6F5F5">
    <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;margin:0;padding:0" bgcolor="#F6F5F5">
      <tbody>
        <tr>
          <td bgcolor="#F6F5F5" valign="top" style="border-collapse:collapse">
            <table width="590" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;min-width:590px" align="center">
              <tbody>
                <tr><td width="44" height="30" style="border-collapse:collapse" bgcolor="#F6F5F5" valign="top"></td></tr>
                <tr>
                  <td width="44" height="60" style="border-collapse:collapse;min-width:44px;color:#222;font-family:Verdana,Helvetica,sans-serif;font-size:32px;font-weight:700;line-height:34px;padding-bottom:15px;text-align:left;padding-left:50px" bgcolor="#F6F5F5" valign="top">
                    <span style="font-size:26px;width:26px;height:26px">🌍</span><?= $title;?>
                  </td>
                </tr>
                <tr>
                  <td width="44" style="border-collapse:collapse;min-width:44px;border-radius:10px" bgcolor="#ffffff" valign="top">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;margin:0;padding:0">
                      <tbody>
                        <tr>
                          <td width="50" height="35" style="border-collapse:collapse;min-width:50px" valign="top"></td>
                          <td width="490" height="35" style="border-collapse:collapse" valign="top"></td>
                          <td width="50" height="35" style="border-collapse:collapse" valign="top"></td>
                        </tr>
                        <tr>
                          <td width="50" height="25" style="border-collapse:collapse;min-width:50px" valign="top"></td>
                          <td width="44" height="25" style="border-collapse:collapse;min-width:50px;text-align: left;" valign="top">
                            <a href="<?= $email_data['domain'];?>" style="font-family:Verdana,Helvetica,sans-serif;font-size:20px;line-height:22px;text-align:left;color:#0075ff;text-decoration-line:underline"><?= $email_data['domain'];?></a>
                          </td>
                          <td width="50" height="25" style="border-collapse:collapse" valign="top"></td>
                        </tr>
                        <tr>
                          <td width="50" height="35" style="border-collapse:collapse;min-width:50px" valign="top"></td>
                          <td width="490" height="35" style="border-collapse:collapse" valign="top"></td>
                          <td width="50" height="35" style="border-collapse:collapse" valign="top"></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td width="44" height="30" style="border-collapse:collapse" bgcolor="#F6F5F5" valign="top"></td>
                </tr>
                <tr>
                  <td width="44" height="60" style="border-collapse:collapse;min-width:44px;color:#222;font-family:Verdana,Helvetica,sans-serif;font-size:32px;font-weight:700;line-height:34px;padding-bottom:15px;text-align:left;padding-left:50px" bgcolor="#F6F5F5" valign="top">
                    <span style="font-size:26px;width:26px;height:26px">💬</span> <?= $body_title;?>
                  </td>
                </tr>
                <tr>
                  <td width="44" style="border-collapse:collapse;min-width:44px;border-radius:10px" bgcolor="#ffffff" valign="top">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;margin:0;padding:0">
                      <tbody>
                        <tr>
                          <td width="50" height="35" style="border-collapse:collapse;min-width:50px" valign="top"></td>
                          <td width="490" height="35" style="border-collapse:collapse" valign="top"></td><td width="50" height="35" style="border-collapse:collapse" valign="top"></td>
                        </tr>
                        <tr>
                          <td width="50" height="40" style="border-collapse:collapse;min-width:50px" valign="top"></td>
                          <td width="44" height="40" style="border-collapse:collapse;min-width:50px;color:#616161;font-family:Verdana,Helvetica,sans-serif;font-size:16px;font-weight:400;line-height:22px" valign="top">
                            <table style="border-collapse:collapse;min-width:50px;color:#616161;font-family:Verdana,Helvetica,sans-serif;font-size:16px;font-weight:400;line-height:22px;width:100%">
                              <tbody>
                                <?php foreach($email_data['fields'] as $field): ?>
                                  <tr>
                                    <td style="font-weight:700;text-align:left;color:#6b6b6b;font-size:18px;font-style:normal;font-weight:400;padding-bottom:5px"><?= $field['label'];?></td>
                                  </tr>
                                  <tr>
                                    <td style="text-align:left;color:#000;font-size:18px;font-style:normal;font-weight:400;padding-bottom:25px">
                                      <span class="wmi-callto"><?= $field['value'];?></span>
                                    </td>
                                  </tr>
                                <?php endforeach; ?>
                                <tr>
                                  <td style="text-align:right;padding-bottom:15px">
                                    <span style="font-weight:700;text-align:right;color:#6b6b6b;font-size:16px;font-style:normal;font-weight:400;padding-bottom:5px"><?= $date_title;?></span> 
                                    <span class="wmi-callto" style="color:#000;font-size:16px;font-style:normal;font-weight:400"><?= $email_data['date'];?></span>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                          <td width="50" height="40" style="border-collapse:collapse" valign="top"></td>
                        </tr>
                        <tr>
                          <td width="50" height="35" style="border-collapse:collapse;min-width:50px" valign="top"></td>
                          <td width="490" height="35" style="border-collapse:collapse" valign="top"></td>
                          <td width="50" height="35" style="border-collapse:collapse" valign="top"></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td width="44" height="25" style="border-collapse:collapse" bgcolor="#F6F5F5" valign="top"></td>
                </tr>
                <tr>
                  <td width="44" height="60" style="border-collapse:collapse;min-width:44px;color:#222;font-family:Verdana,Helvetica,sans-serif;font-size:16px;font-weight:700;line-height:22px" bgcolor="#F6F5F5" valign="bottom">
                    <table border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;margin:0;padding:0">
                      <tbody>
                        <tr>
                          <td width="25" height="30" style="border-collapse:collapse;min-width:25px" valign="top"></td>
                          <td width="540" height="30" style="text-align:center;border-collapse:collapse;min-width:540px;color:#000;font-family:Verdana,Helvetica,sans-serif;font-size:20px;font-weight:400;line-height:22px" valign="top"><?= $team_title;?>
                            <a href="<?= $company_link;?>" style="font-weight:700;padding-left:5px;width:185px;height:25px;text-decoration:none;">
                              <img src="<?= $company_logo;?>" style="width:185px;height:25px;">
                            </a>
                          </td>
                          <td width="25" height="30" style="border-collapse:collapse;min-width:25px" valign="top"></td>
                        </tr>
                        <tr>
                          <td width="25" height="30" style="border-collapse:collapse;min-width:25px" valign="top"></td>
                          <td width="540" height="30" style="padding-top:10px;text-align:center;border-collapse:collapse;min-width:540px;color:#6b6b6b;font-family:Verdana,Helvetica,sans-serif;font-size:16px;font-weight:400;line-height:22px" valign="top"><?= $thanks_message;?> ❤</td>
                          <td width="25" height="30" style="border-collapse:collapse;min-width:25px" valign="top"></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
<?php endif; ?>