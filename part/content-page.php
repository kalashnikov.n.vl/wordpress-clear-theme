<?php get_header(); ?>
<div id="content" class="site-content">
  <?php the_content();?>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
