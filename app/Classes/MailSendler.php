<?php

namespace App\Classes;

class MailSendler{
  public function __construct(){
    $this->mail_action = 'send_mail';
    $this->nonce_slug = 'convert_mail';
    
    add_action('wp_ajax_'.$this->mail_action, array($this, 'convertmeSendEmail'));
    add_action('wp_ajax_nopriv_'.$this->mail_action, array($this, 'convertmeSendEmail'));
  }

  public function convertmeSendEmail(){
    if (!isset($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], $this->nonce_slug)) {
      wp_send_json_error('Invalid nonce');
      die();
    }

    date_default_timezone_set('Europe/Moscow');

    $success_message = __('Письмо успешно отправленно', THEME_NAME);
    $error_message = __('Ошибка отправки письма', THEME_NAME);

    // Field data
    $tel = sanitize_text_field($_POST['tel']) ?? '';
    $email = sanitize_text_field($_POST['email']) ?? '';
    
    $email_data = [
      'fields' => [],
      'date' => date("d.m.Y G:i"), 
      'domain' => $_SERVER['SERVER_NAME'],
    ];

    if(!empty($tel)){
      $email_data['fields'] = [
        'label' => __('Телефон', THEME_NAME),
        'value' => $tel,
      ];
    };

    if(!empty($email)){
      $email_data['fields'] = [
        'label' => __('Email', THEME_NAME),
        'value' => $email,
      ];
    };

    // Email settings
    $email_to = sanitize_text_field($_POST['email_to']) ?? '';
    $email_subject = isset($_POST['subject']) ? sanitize_text_field($_POST['subject']) : __('Новая заявка', THEME_NAME);
    $body = $this->convertmeGetEmailTemplate($email_data);
    $headers = array('Content-Type: text/html; charset=UTF-8');
  
    // Send email
    $success = wp_mail($email_to, $email_subject, $body, $headers);
  
    // Return result
    if ($success) {
      wp_send_json_success($success_message);
    } else {
      wp_send_json_error($error_message);
    }
    
    wp_die();
  }

  private function convertmeGetEmailTemplate($email_data){
    ob_start();
    get_template_part('part/email/email', null, array('email_data' => $email_data));
    return ob_get_clean();
  }
}