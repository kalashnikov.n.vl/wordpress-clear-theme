<?php

namespace App\Classes;

class RegisterPostClass
{
  /**
   * Register custom taxonomy
   */
  public function __construct(){
    // add_action('init', [$this, 'exampleCustomTaxonomy']);
  }

  public function exampleCustomTaxonomy(){  
    $labels = array(  
      'name' => __('Пример', 'ibcon'),
      'singular_name' => __('Пример','ibcon'),
      'add_new' => __('Добавить пример','ibcon'),
      'add_new_item' => __('Новый пример','ibcon'),
      'edit_item' => __('Редактировать пример','ibcon'),
      'new_item' => __('Новый пример','ibcon'),
      'view_item' => __('Смотреть пример','ibcon'),
      'search_items' => __('Найти пример','ibcon'),
      'not_found' => __('Пример не найденно','ibcon'),
      'not_found_in_trash' => __('В корзине примеров не найденно','ibcon'),
      'parent_item_colon' => '',
      'menu_name' => __('Пример','ibcon'),  
    );  
    $args = array(  
      'labels' => $labels,  
      'public' => true,  
      'publicly_queryable' => true,  
      'show_ui' => true,  
      'show_in_menu' => true,  
      'query_var' => false,  
      'rewrite' => array('slug' => 'example','with_front' => false),
      'capability_type' => 'post',  
      'has_archive' => true,  
      'hierarchical' => false,  
      'show_in_rest' => true,
      'menu_position' => 5,
      'menu_icon' => 'dashicons-example',
      'supports' => array('title','editor','thumbnail','custom-fields','excerpt',)
    );  
    register_post_type( 'example', $args );  
    register_taxonomy("example_categories", array("example"), array("hierarchical" => true, "label" => "Категории примеров", "singular_label" => "Categories", 'show_in_rest' => true, "rewrite" => array( 'slug' => 'example-categories', 'with_front'=> false )));
  }
}