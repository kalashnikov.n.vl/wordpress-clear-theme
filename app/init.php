<?php

foreach (glob(__DIR__ . "/Helpers/*.php") as $filename) {
  include $filename;
}

foreach (glob(__DIR__ . "/Classes/*.php") as $filename) {
  include $filename;
}

new App\Classes\GutenbergInitClass();
new App\Classes\RegisterPostClass();


// Init front class

if (!is_admin()){
  new App\Classes\MailSendler();
}